const express = require('express');
const router = express.Router();
const controller = require('../controllers/userController');
const routeControl = require('../routeControl');

router.get('/', controller.getInfo, routeControl.endRequest);

module.exports = router;