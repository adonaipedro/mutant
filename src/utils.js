exports.quickSort = (list) => {

  let websites = {};
  let suites = {};

  function swap(items, leftIndex, rightIndex) {
    let temp = items[leftIndex];
    items[leftIndex] = items[rightIndex];
    items[rightIndex] = temp;
  }
  function partition(items, left, right) {
    let pivot = items[Math.floor((right + left) / 2)],
      i = left,
      j = right;




    if (pivot.address && pivot.address.suite) {
      if (!(pivot.id in suites)) {
        suites[pivot.id] =
          {
            suite: pivot.address.suite,
            name: pivot.name
          };
      }
    }


    if (!(pivot.id in websites)) {
      websites[pivot.id] =
        {
          website: pivot.website,
          name: pivot.name
        };
    }

    while (i <= j) {

      while (compareNamesLess(items[i].name, pivot.name)) {

        if (!(items[i].id in websites)) {
          websites[items[i].id] =
            {
              website: items[i].website,
              name: items[i].name
            };
        }

        if (items[i].address && items[i].address.suite) {
          if (!(items[i].id in suites)) {
            suites[items[i].id] =
              {
                suite: items[i].address.suite,
                name: items[i].name
              };
          }
        }
        i++;
      }


      while (compareNamesGreater(items[j].name, pivot.name)) {

        if (!(items[j].id in websites)) {
          websites[items[j].id] =
            {
              website: items[j].website,
              name: items[j].name
            };
        }

        if (items[j].address && items[j].address.suite) {
          if (!(items[j].id in suites)) {
            suites[items[j].id] =
              {
                suite: items[j].address.suite,
                name: items[j].name
              };
          }
        }
        j--;
      }

      if (i <= j) {
        swap(items, i, j);
        i++;
        j--;
      }

    }
    return i;
  }

  function quickSort(items, left, right) {
    let index;
    if (items.length > 1) {
      index = partition(items, left, right);
      if (left < index - 1) {
        quickSort(items, left, index - 1);
      }
      if (index < right) {
        quickSort(items, index, right);
      }
    }
    return { items, websites, suites };
  }

  return quickSort(list, 0, list.length - 1);
}


function compareNamesGreater(name1, name2) {
  name1 = proccessName(name1);
  name2 = proccessName(name2);

  let name1Array = name1.split(' ');
  let name2Array = name2.split(' ');

  if (name1Array[0] == name2Array[0] && (name1Array.length > 1 && name2Array.length > 1)) {
    return name1Array[1] > name2Array[1];
  } else {
    return name1Array[0] > name2Array[0];
  }

}

function compareNamesLess(name1, name2) {
  name1 = proccessName(name1);
  name2 = proccessName(name2);

  let name1Array = name1.split(' ');
  let name2Array = name2.split(' ');

  if (name1Array[0] == name2Array[0] && (name1Array.length > 1 && name2Array.length > 1)) {
    return name1Array[1] < name2Array[1];
  } else {
    return name1Array[0] < name2Array[0];
  }

}

function proccessName(name) {
  if (name.indexOf('Mr.') > -1) {
    name = name.split('Mr.').pop();
  }

  if (name.indexOf('Mrs.') > -1) {
    name = name.split('Mrs.').pop();
  }
  name = name.trimLeft();
  return name;
}