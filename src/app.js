const express = require('express');
var helmet = require('helmet');
var cors = require('cors');
const app = express();


//Rotas
const userRoute = require('./routes/userRoute');
app.use('/user', userRoute);

app.use(helmet());
app.use(cors({
  origin: true,
  methods: ['GET', 'POST'],
  credentials: true
}));

app.get('/', function(req,res) {
  res.sendfile('public/index.html');
});

app.use(express.static('public'));

app.listen(8080, function () {
  console.log('app listening on port 8080')
})