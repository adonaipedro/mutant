var rp = require('request-promise');

exports.getAll = () => {
    let options = {
        method: 'GET',
        url: 'https://jsonplaceholder.typicode.com/users',
        headers:
        {
            'Content-Type': 'application/json'
        }
    };

    return rp(options);
}