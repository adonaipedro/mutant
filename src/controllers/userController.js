const repository = require('../repositories/userRepository');
const utils = require('../utils');

exports.getInfo = (req, res, next) => {


  repository.getAll().then((result) => {
    let resultProccess = utils.quickSort(JSON.parse(result));

    res.locals.websites = resultProccess.websites;
    res.locals.suites = resultProccess.suites;
    res.locals.users = resultProccess.items;
    return next();
  }).catch((exception) => {
    console.log(exception);
    if (exception.error && exception.error.code)
      res.locals.error = exception.error.code;
    else
      res.locals.error = 'error';
    return next();
  });

};


