#docker build -t mutantproject .
#sudo docker run -d -it --restart always -p 8080:8080 mutantproject

FROM node:10
EXPOSE 8080

COPY package*.json ./
COPY src src/
COPY public public/

RUN npm install

CMD [ "npm", "start" ]
