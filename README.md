﻿# MutantProject
###Instruções
- **Instalar docker:**
https://docs.docker.com/install/

- **Clonar o repositório com:**
git clone https://adonaipedro@bitbucket.org/adonaipedro/mutant.git

-  **Criar a imagem do docker com:**
docker build -t mutantproject .

-  **Executar com:**
sudo docker run -d -it --restart always -p 8080:8080 mutantproject

-   **Acessar aplicação em:**
http://localhost:8080

